﻿using System.Collections;

class Program
{
    static void Main(string[] args)
    {
        // Фильтр Блума
        Bloom bloom = new Bloom();
        bloom.Add("string");
        bloom.Add("fresh");
        bloom.Add("basketball");
        bloom.Add("chess");
        bloom.Add("running");
        bloom.CheckFor("basketball");
        bloom.CheckFor("soccer");
        bloom.CheckFor("chess");
        bloom.CheckFor("run");
        Console.ReadLine();


        // MinHash
        // Создаем два объекта MinHash с 100 функциями хеширования и произвольным семям
        var minHash1 = new MinHash(100, 123);
        var minHash2 = new MinHash(100, 123);

        // Добавляем слова из первой строки
        foreach (var word in "Hello, world!".Split(' '))
        {
            minHash1.AddElement(word);
        }

        // Добавляем слова из второй строки
        foreach (var word in "Hello, world!".Split(' '))
        {
            minHash2.AddElement(word);
        }

        // Сравниваем MinHashы двух строк
        bool areSimilar = minHash1.IsEqual(minHash2);
        Console.WriteLine($"Строки похожи: {areSimilar}");
    }
}


class Bloom
{
    private BitArray bits = new BitArray(32);

    private short RotateMore(string AddString)
    {
        short ReturnValue = 0;
        for (int i = 0; i < AddString.Length; i++)
        {
            ReturnValue += (short)(AddString[i] * i);
            ReturnValue = (short)(ReturnValue % 32);
        }
        return ReturnValue;
    }

    private short Rotate(string AddString)
    {
        short ReturnValue = 0;
        for (int i = 0; i < AddString.Length; i++)
        {
            ReturnValue += (short)(int)AddString[i];
            ReturnValue = (short)(ReturnValue % 32);
        }
        return ReturnValue;
    }

    public void Add(string AddString)
    {
        Console.WriteLine("adding " + AddString);

        short Point1 = Rotate(AddString);
        short Point2 = RotateMore(AddString);
        bits[Point1] = true;
        bits[Point2] = true;
    }

    public bool Contains(string CheckString)
    {
        short Point1 = Rotate(CheckString);
        short Point2 = RotateMore(CheckString);
        if (bits[Point1] && bits[Point2])
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void CheckFor(string key)
    {
        if (Contains(key))
        {
            Console.WriteLine(key + " may be in there");
        }
        else
        {
            Console.WriteLine(key + " is not there");
        }
    }
}


public class MinHash
{
    private readonly uint[] _hashValues;
    private readonly int[] _seeds;

    public MinHash(uint numHashFunctions, int randomSeed)
    {
        _hashValues = new uint[numHashFunctions];
        _seeds = new int[numHashFunctions];
        var rand = new Random(randomSeed);
        for (int i = 0; i < numHashFunctions; i++)
        {
            _seeds[i] = rand.Next();
        }
    }

    public void AddElement(string element)
    {
        for (int i = 0; i < _hashValues.Length; i++)
        {
            _hashValues[i] = _hashValues[i] > Hash(element, _seeds[i]) ? _hashValues[i] : Hash(element, _seeds[i]);
        }
    }

    public bool IsEqual(MinHash other)
    {
        for (int i = 0; i < _hashValues.Length; i++)
        {
            if (_hashValues[i] != other._hashValues[i])
            {
                return false;
            }
        }

        return true;
    }

    private static uint Hash(string element, int seed)
    {
        unchecked
        {
            const uint p = 16777619;
            var hash = 2166136261;

            for (int i = 0; i < element.Length; i++)
            {
                hash = (hash ^ element[i]) * p;
            }

            hash += hash << 13;
            hash ^= hash >> 7;
            hash += hash << 3;
            hash ^= hash >> 17;
            hash += hash << 5;

            var result = hash % (uint)(seed + 1);
            return result;
        }
    }
}




